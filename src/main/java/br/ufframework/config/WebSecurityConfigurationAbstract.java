package br.ufframework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import br.ufframework.domain.Permissao;
import br.ufframework.service.AbstractAuthProviderService;


/**
 * Classe abstrata para configuração do spring security
 * 
 * A classe do projeto que extender essa classe abstrata tera que colocar as seguintes anotações na classe: 
 * @Configuration
 * @EnableWebSecurity
 *
 */

public abstract class WebSecurityConfigurationAbstract extends WebSecurityConfigurerAdapter {
	
	protected abstract AbstractAuthProviderService<?> getAuthProvider();

    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(getAuthProvider());
    }
	
    /**
     * configuração basica do spring security
     */
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/autenticacao").successForwardUrl(pageLoginSucess())
                .defaultSuccessUrl(pageLoginSucess(), true)        
                .permitAll()
                .and()
            .logout().logoutSuccessUrl(pageLogoutSucess())
                .permitAll();
    }
	
	/**
	 * Pagina que sera redirecionada quando o login tiver sucesso.
	 * @return
	 */
	protected abstract String pageLoginSucess();
	
	/**
	 * Pafina que sera redirecionada quando o logout tiver sucesso.
	 * @return
	 */
	protected abstract String pageLogoutSucess();

}
