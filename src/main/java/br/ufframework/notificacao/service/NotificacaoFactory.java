package br.ufframework.notificacao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufframework.domain.TipoDeNotificador;
import br.ufframework.telegram.handler.NotificadorTelegram;

/**
 * Fábrica de notificadores do framework.
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public class NotificacaoFactory {
	
	@Autowired
	private NotificadorTelegram notificadorTelegram;
	
	@Autowired
	private NotificadorEmail notificadorEmail;
	
	public Notificador getNotificador(TipoDeNotificador tipo) {
		switch (tipo) {
		case EMAIL:
			return notificadorEmail;
		case TELEGRAM:
			return notificadorTelegram;
		default:
			return null;
		}
	}

}
