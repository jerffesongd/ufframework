package br.ufframework.notificacao.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import br.ufframework.domain.Notificacao;
import br.ufframework.domain.Usuario;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public class NotificadorEmail extends EmailService implements Notificador {

	@Override
	public void notificarUsuarios(Notificacao notificacao, Collection<? extends Usuario> usuarios) {
		if (notificacao != null && usuarios != null) {
			List<String> emails = new ArrayList<String>();
			for (Usuario usuario: usuarios) {
				if (usuario.getEmail() != null) {
					emails.add(usuario.getEmail());
				}
			}
			
			if (!emails.isEmpty())
				enviarMensagem(emails.toArray(new String[emails.size()]), "Nova notificação.", notificacao.getMensagem());
		}
	}

}
