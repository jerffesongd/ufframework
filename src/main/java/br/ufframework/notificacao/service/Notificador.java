package br.ufframework.notificacao.service;

import java.util.Collection;

import br.ufframework.domain.Notificacao;
import br.ufframework.domain.Usuario;

/**
 * Abstração dos serviços responsáveis pelo envio de notificações.
 * 
 * @author Lucas Aléssio
 *
 */
public interface Notificador {
	
	public void notificarUsuarios(Notificacao notificacao, Collection<? extends Usuario> usuarios);

}
