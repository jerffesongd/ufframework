package br.ufframework.notificacao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public class EmailService {

	@Autowired
    public JavaMailSender emailSender;
 
    public void enviarMensagem(String destinatario, String assunto, String texto) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(destinatario); 
        message.setSubject(assunto); 
        message.setText(texto);
        emailSender.send(message);
    }

    public void enviarMensagem(String[] destinatarios, String assunto, String texto) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(destinatarios); 
        message.setSubject(assunto); 
        message.setText(texto);
        emailSender.send(message);
    }
	
}
