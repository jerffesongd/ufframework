package br.ufframework.notificacao.service;

import java.util.Collection;

import org.springframework.stereotype.Component;

import br.ufframework.domain.ForumOperacoes;
import br.ufframework.domain.Usuario;
import br.ufframework.domain.Notificacao;
import br.ufframework.domain.Post;
import br.ufframework.domain.RespostaPost;

/**
 * Classe a ser extendida pelos notificadores da aplicação.
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public abstract class AbstractNotificacaoService {
	
	protected Notificacao notificacao;
	
	public void notificar(ForumOperacoes operacao, Post post, RespostaPost resposta, Collection<? extends Usuario> usuarios) {
		switch (operacao) {
		case CADASTRAR_POST:
			criarNotificacaoPostCadastrado(post);
		case RESPONDER_POST:
			criarNotificacaoPostRespondido(resposta);
		}
		
		if (notificacao != null && getNotificador() != null)
			getNotificador().notificarUsuarios(notificacao, usuarios);
	}

	/**
	 * Define o notificador utilizado pelo serviço
	 * @return
	 */
	public abstract Notificador getNotificador();
	
	/**
	 * Define o conteudo da Notificacao para o cadastro de Post
	 */
	public abstract void criarNotificacaoPostCadastrado(Post post);
	
	/**
	 * Define o conteudo da Notificacao para uma RespostaPost
	 */
	public abstract void criarNotificacaoPostRespondido(RespostaPost resposta);
	
}
