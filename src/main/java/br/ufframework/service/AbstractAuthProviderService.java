package br.ufframework.service;


import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.ufframework.domain.Usuario;
import br.ufframework.repository.UsuarioRepository;


/**
 * 
 * @author Lucio Soares de Oliveira
 *
 * @param <T>
 */

public abstract class AbstractAuthProviderService<T extends Usuario> implements AuthenticationProvider {
	
	@Autowired
	protected abstract UsuarioRepository<T> getUsuarioRepository();
	
    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {    	
    	String login = auth.getName();
        String senha = auth.getCredentials().toString();
        T usuario = getUsuarioRepository().findByLogin(login);
    	   if (usuario != null) {
               if (verificarSenha(usuario, senha)) {
                   Collection<? extends GrantedAuthority> authorities = usuario.getPermissoes();
                   return new UsernamePasswordAuthenticationToken(login, senha, authorities);
               } else {
                   throw new BadCredentialsException("Usuário e/ou senha inválidos..");
               }
           }
           throw new UsernameNotFoundException("Usuário e/ou senha inválidos.");  
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
    protected boolean verificarSenha(T usuario, String senha){
    	if(usuario.getSenha().equals(senha))
    		return true;
    	return false;
    }
	
	
	
}