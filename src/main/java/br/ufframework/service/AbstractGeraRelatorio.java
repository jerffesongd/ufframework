package br.ufframework.service;

import java.util.Map;

public interface AbstractGeraRelatorio {
	public Map<String, Integer> gerar();
}
