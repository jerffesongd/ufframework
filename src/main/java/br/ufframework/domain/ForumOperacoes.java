package br.ufframework.domain;

/**
 * Operações realizadas pela extensão do PostController
 * 
 * @author Lucas Aléssio
 *
 */
public enum ForumOperacoes {

	CADASTRAR_POST,
	RESPONDER_POST;
	
}
