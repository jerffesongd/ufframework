package br.ufframework.domain;

/**
 * Métodos de chamada a api do telegram
 * 
 * @author Lucas Alessio
 *
 */
public enum Method {

	SEND_MESSAGE ("/sendMessage"),
	
	EDIT_MESSAGE_TEXT ("/editMessageText"),
	
	DELETE_MESSAGE ("/deleteMessage"),
	
	ANSWER_CALLBACK_QUERY ("/answerCallbackQuery");
	
	private String value;
		
	Method(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
