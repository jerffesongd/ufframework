package br.ufframework.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="telegram", name="user_context")
public class UserContext {

	@Id
	@Column(name="id_user")
	private Integer idUser;
	
	@Column(name="id_usuario_externo")
	private Integer idUsuarioExterno;
	
	@Column(name="current_state")
	private States currentState;
	
	@Column(name="id_grupo")
	private Integer idGrupo;

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getIdUsuarioExterno() {
		return idUsuarioExterno;
	}

	public void setIdUsuarioExterno(Integer idUsuarioExterno) {
		this.idUsuarioExterno = idUsuarioExterno;
	}

	public States getCurrentState() {
		return currentState;
	}

	public void setCurrentState(States currentState) {
		this.currentState = currentState;
	}

	public Integer getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}
	
}
