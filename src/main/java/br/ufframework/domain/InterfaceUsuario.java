package br.ufframework.domain;

/**
 * Interface para representar os usuários do sistema.
 * 
 * @author Lucas Aléssio
 *
 */
public interface InterfaceUsuario {

	public Integer getId();
	
	public String getNome();
	
}
