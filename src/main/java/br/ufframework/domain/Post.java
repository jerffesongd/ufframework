package br.ufframework.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;


@Entity
public class Post extends AbstractEntity{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_POST")
	@SequenceGenerator(name="SEQ_POST", sequenceName="ID_SEQ_POST", allocationSize=1)
	@Column(name="id_post")
	private Integer id;

	@NotNull
	@Column(columnDefinition="text")
	private String titulo;
	
	@Column
	private Integer visualizacoes = 0;

	@NotNull
	@Column(columnDefinition="text")
	private String conteudo;
	
	@ManyToOne
	@JoinColumn(name = "id_grupo")
	private Grupo grupo;

	private String status;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario responsavel;

	@OneToMany(mappedBy = "post", targetEntity=RespostaPost.class)
	private List<RespostaPost> respostas = new ArrayList<RespostaPost>();
	
	public Integer getVisualizacoes() {
		return visualizacoes;
	}

	public void setVisualizacoes(Integer visualizacoes) {
		this.visualizacoes = visualizacoes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	public Usuario getResponsavel() {
		
		return responsavel;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public List<RespostaPost> getRespostas() {
		return respostas;
	}


	public Grupo getGrupo() {
		
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
