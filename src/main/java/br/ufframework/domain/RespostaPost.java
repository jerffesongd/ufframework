package br.ufframework.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="resposta_post")
public class RespostaPost extends AbstractEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_RESPOSTA_POST")
	@SequenceGenerator(name="SEQ_RESPOSTA_POST", sequenceName="ID_SEQ_RESPOSTA_POST", allocationSize=1)
	@Column(name="id_resposta_post")
	private Integer id;
	
	@NotNull
	@Column(columnDefinition="text")
	private String conteudo;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario responsavel;
	
	@NotNull
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=Post.class)
	@JoinColumn(name = "id_post")
	private Post post;
	
	private String status;


	public Integer getId() {
		return id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Usuario getResponsavel() {
		return responsavel;
	}
	
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
