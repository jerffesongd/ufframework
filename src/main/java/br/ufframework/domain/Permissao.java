package br.ufframework.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;

@Entity
public class Permissao implements GrantedAuthority {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_PERMISSAO")
	@SequenceGenerator(name="SEQ_PERMISSAO", sequenceName="ID_SEQ_PERMISSAO", allocationSize=1)
	@Column(name="id_PERMISSAO")
	private Integer id;

	@NotNull
	@Column(columnDefinition="text")
	private String codigo;
	
	@NotNull
	@Column(columnDefinition="text")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return codigo;
	}
	
}
