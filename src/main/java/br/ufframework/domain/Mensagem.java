package br.ufframework.domain;

/**
 * Entidade que reprensenta uma mensagem de aviso ao usuario.
 *
 */

public class Mensagem implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String LAYOUT_ATRIBUTO_MENSAGEM = "layoutMensagem";

	/**
	 * Tipos das mensagens.
	 * @author luciooliveira
	 *
	 */
	public static enum Tipo {
		DANGER, WARNING, INFO, SUCCESS;
	}
	/**
	 * Atributo que representa o conteudo da mensagem
	 */
	private final String conteudo;
	/**
	 * Atributo que representa o tipo da mensagem
	 */
	private final Tipo tipo;
	private final Object[] args;

	public Mensagem(String conteudo, Tipo tipo) {
		this.conteudo = conteudo;
		this.tipo = tipo;
		this.args = null;
	}

	public Mensagem(String conteudo, Tipo tipo, Object... args) {
		this.conteudo = conteudo;
		this.tipo = tipo;
		this.args = args;
	}

	public String getConteudo() {
		return conteudo;
	}

	public String getTipo() {
		return tipo.toString().toLowerCase();
	}

	public Object[] getArgs() {
		return args;
	}

}
