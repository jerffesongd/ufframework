package br.ufframework.domain;

/**
 * Tipos de notificadores suportados pelo framework.
 * 
 * @author Lucas Aléssio
 *
 */
public enum TipoDeNotificador {
	
	TELEGRAM,
	EMAIL;

}
