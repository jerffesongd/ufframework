package br.ufframework.domain;

public enum StatusCadastro {

	EM_CADASTRO ("EM CADASTRO"),
	CADASTRADO ("CADASTRADO");
	
	private String value;
	
	StatusCadastro(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
