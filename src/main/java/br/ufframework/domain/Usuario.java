package br.ufframework.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable, InterfaceUsuario{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_USUARIO")
	@SequenceGenerator(name="SEQ_USUARIO", sequenceName="ID_SEQ_USUARIO", allocationSize=1)
	@Column(name="id_usuario")
	private Integer idUsuario;

	@Column(name="nome_pessoa")
	private String nomePessoa;
	
	@Column(name="login", nullable=true)
	private String login;
	
	@ManyToMany(fetch=FetchType.EAGER)
   	@JoinTable(name="permissao_user", schema="public", joinColumns = @JoinColumn(name="id_usuario"), inverseJoinColumns = @JoinColumn(name="id_permissao"))
	private List<Permissao> permissoes;
	
	@Column(name="senha", nullable=true)
	private String senha;

	private String email;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public Integer getId() {
		return idUsuario;
	}

	@Override
	public String getNome() {
		return nomePessoa;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Permissao> getPermissoes() {
		
		return permissoes;
	}
	
}
