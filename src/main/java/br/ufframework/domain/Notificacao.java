package br.ufframework.domain;

/**
 * Abstração de uma notificação do sistema.
 * 
 * @author Lucas Aléssio
 *
 */
public interface Notificacao {
	
	public String getMensagem();

}
