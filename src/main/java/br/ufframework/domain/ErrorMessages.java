package br.ufframework.domain;

public enum ErrorMessages {

	USUARIO_NAO_AUTENTICADO ("Por favor, autentique-se pelo site."),
	USUARIO_NAO_CADASTROU_POSTS ("Você ainda não cadastrou nenhum post."),
	GRUPO_NAO_TEM_POSTS ("Não há posts cadastrados."),
	CODIGO_INVALIDO ("Código inválido."),
	ERROR ("Erro!"),
	GENERIC_ERROR ("Ainda não sei responder isso.");
	
	private String message;
	
	ErrorMessages(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
