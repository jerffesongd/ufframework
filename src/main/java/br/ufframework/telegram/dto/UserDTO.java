package br.ufframework.telegram.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.ufframework.domain.InterfaceUsuario;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Entity
@Table(schema="telegram", name="user")
public class UserDTO implements InterfaceUsuario {

	@Id
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("is_bot")
	private boolean bot;
	
	@JsonProperty("first_name")
	private String firstName;
	
	@JsonProperty("last_name")
	private String lastName;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("language_code")
	private String languageCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isBot() {
		return bot;
	}

	public void setBot(boolean bot) {
		this.bot = bot;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	@Override
	public String getNome() {
		return firstName;
	}
	
}
