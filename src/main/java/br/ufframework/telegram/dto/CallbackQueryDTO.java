package br.ufframework.telegram.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(schema="telegram", name="callback_query")
public class CallbackQueryDTO {

	@Id
	@JsonProperty("id")
	private String id;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_message", referencedColumnName="id")
	@JsonProperty("message")
	private MessageDTO message;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_user", referencedColumnName="id")
	@JsonProperty("from")
	private UserDTO user;

	@JsonProperty("data")
	private String data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MessageDTO getMessage() {
		return message;
	}

	public void setMessage(MessageDTO message) {
		this.message = message;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
