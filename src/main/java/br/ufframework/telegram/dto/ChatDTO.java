package br.ufframework.telegram.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Entity
@Table(schema="telegram", name="chat")
public class ChatDTO {

	@Id
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("type")
	private String type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
