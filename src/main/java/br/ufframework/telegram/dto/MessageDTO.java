package br.ufframework.telegram.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.ufframework.domain.Notificacao;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Entity
@Table(schema="telegram", name="message")
public class MessageDTO implements Notificacao {

	@Id
	@JsonProperty("message_id")
	private Integer id;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_user", referencedColumnName="id")
	@JsonProperty("from")
	private UserDTO user;
	
	@JsonProperty("date")
	private Integer date;
	
	@JsonProperty("text")
	@Column(columnDefinition="text")
	private String text;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_chat", referencedColumnName="id")
	@JsonProperty("chat")
	private ChatDTO chat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ChatDTO getChat() {
		return chat;
	}

	public void setChat(ChatDTO chat) {
		this.chat = chat;
	}

	@Override
	public String getMensagem() {
		return text;
	}

}
