package br.ufframework.telegram.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Entity
@Table(schema="telegram", name="update")
public class UpdateDTO {

	@Id
	@JsonProperty("update_id")
	private Integer id;

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="id_message", referencedColumnName="id")
	@JsonProperty("message")
	private MessageDTO message;

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="id_edited_message", referencedColumnName="id")
	@JsonProperty("edited_message")
	private MessageDTO editedMessage;

	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="id_callback_query", referencedColumnName="id")
	@JsonProperty("callback_query")
	private CallbackQueryDTO callbackQuery;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MessageDTO getMessage() {
		return message;
	}

	public void setMessage(MessageDTO message) {
		this.message = message;
	}

	public MessageDTO getEditedMessage() {
		return editedMessage;
	}

	public void setEditedMessage(MessageDTO editedMessage) {
		this.editedMessage = editedMessage;
	}

	public CallbackQueryDTO getCallbackQuery() {
		return callbackQuery;
	}

	public void setCallbackQuery(CallbackQueryDTO callbackQuery) {
		this.callbackQuery = callbackQuery;
	}

}
