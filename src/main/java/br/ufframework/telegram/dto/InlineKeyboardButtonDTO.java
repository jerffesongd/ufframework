package br.ufframework.telegram.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Lucas Alessio
 *
 */
public class InlineKeyboardButtonDTO {
	
	@JsonProperty("text")
	private String text;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("url")
	private String url;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("callback_data")
	private String callbackData;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCallbackData() {
		return callbackData;
	}

	public void setCallbackData(String callbackData) {
		this.callbackData = callbackData;
	}

}
