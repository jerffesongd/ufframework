package br.ufframework.telegram.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TelegramResponseDTO {

	@JsonProperty("result")
	private MessageDTO message;

	public MessageDTO getMessage() {
		return message;
	}

	public void setMessage(MessageDTO message) {
		this.message = message;
	}
	
}
