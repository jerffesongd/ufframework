package br.ufframework.telegram.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Lucas Alessio
 *
 */
public class InlineKeyboardMarkupDTO {

	@JsonProperty("inline_keyboard")
	private InlineKeyboardButtonDTO[][] buttons;

	public InlineKeyboardButtonDTO[][] getButtons() {
		return buttons;
	}

	public void setButtons(InlineKeyboardButtonDTO[][] buttons) {
		this.buttons = buttons;
	}
	
}
