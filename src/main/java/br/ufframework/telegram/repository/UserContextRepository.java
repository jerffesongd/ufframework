package br.ufframework.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.domain.UserContext;

public interface UserContextRepository extends JpaRepository<UserContext, Integer> {

	@Query(value="from UserContext where idUser = :idUser ")
	public UserContext findUserContextByIdUser(@Param("idUser") Integer idUser);
	
}
