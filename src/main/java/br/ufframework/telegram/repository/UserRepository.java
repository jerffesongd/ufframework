package br.ufframework.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufframework.telegram.dto.UserDTO;

public interface UserRepository extends JpaRepository<UserDTO, Integer> {

}
