package br.ufframework.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.telegram.dto.ChatDTO;

public interface ChatRepository extends JpaRepository<ChatDTO, Integer> {

	@Query(value="select ch.* from telegram.message me "
			+ "join telegram.user_context uc on (uc.id_user = me.id_user) "
			+ "join telegram.chat ch on (ch.id = me.id_chat) "
			+ "where uc.id_usuario_externo = :idUser order by me.id desc limit 1", nativeQuery=true)
	public ChatDTO findLastChatByExternalUser(@Param("idUser") Integer idUser);
	
}
