package br.ufframework.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.telegram.dto.MessageDTO;

public interface MessageRepository extends JpaRepository<MessageDTO, Integer> {

	@Query(value="select me.* from telegram.message me "
			+ "join telegram.user u on (u.id = me.id_user) "
			+ "where me.id_chat = :idChat and u.bot is :isBot order by me.id desc limit 1", nativeQuery=true)
	public MessageDTO findLastMessageByChat(@Param("idChat") Integer idChat, @Param("isBot") boolean isBot);
	
}
