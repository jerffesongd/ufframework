package br.ufframework.telegram.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufframework.telegram.dto.UpdateDTO;

public interface UpdateRepository extends JpaRepository<UpdateDTO, Integer> {

}
