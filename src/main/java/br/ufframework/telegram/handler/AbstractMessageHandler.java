package br.ufframework.telegram.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.ufframework.domain.Method;
import br.ufframework.telegram.APITelegramConfig;
import br.ufframework.telegram.dto.MessageDTO;
import br.ufframework.telegram.dto.TelegramResponseDTO;
import br.ufframework.telegram.repository.MessageRepository;

/**
 * Classe a ser estendida por qualquer outra classe 
 * que envie/receba mensagens da API do Telegram.
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public class AbstractMessageHandler {
	
	@Autowired
	private APITelegramConfig apitelegramConfig;

	@Autowired
	private MessageRepository messageRepository;

	public MessageDTO sendMessage(Integer chatId, String text, Integer replyTo, String replyMarkup) {
		Method method = Method.SEND_MESSAGE;

		RestTemplate restTemplate = new RestTemplate();
		String url = apitelegramConfig.getUrlBase() + apitelegramConfig.getKey() + method.getValue();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		builder.queryParam("chat_id", chatId);
		builder.queryParam("text", text);
		builder.queryParam("parse_mode", "HTML");
		if (replyTo != null)
			builder.queryParam("reply_to_message_id", replyTo);
		if (replyMarkup != null)
			builder.queryParam("reply_markup", replyMarkup);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<TelegramResponseDTO> response = restTemplate.postForEntity(builder.build().encode().toUri(), requestEntity, TelegramResponseDTO.class);
		messageRepository.save(response.getBody().getMessage());
		return response.getBody().getMessage();
	}

	public MessageDTO editMessage(Integer chatId, String text, Integer messageId, String replyMarkup) {
		Method method = Method.EDIT_MESSAGE_TEXT;

		RestTemplate restTemplate = new RestTemplate();
		String url = apitelegramConfig.getUrlBase() + apitelegramConfig.getKey() + method.getValue();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		builder.queryParam("chat_id", chatId);
		builder.queryParam("message_id", messageId);
		builder.queryParam("text", text);
		builder.queryParam("parse_mode", "HTML");
		if (replyMarkup != null)
			builder.queryParam("reply_markup", replyMarkup);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<TelegramResponseDTO> response = restTemplate.postForEntity(builder.build().encode().toUri(), requestEntity, TelegramResponseDTO.class);
		messageRepository.save(response.getBody().getMessage());
		return response.getBody().getMessage();
	}

	public void answerCallbackQuery(String callbackQueryId, String text, boolean showAlert) {
		Method method = Method.ANSWER_CALLBACK_QUERY;

		RestTemplate restTemplate = new RestTemplate();
		String url = apitelegramConfig.getUrlBase() + apitelegramConfig.getKey() + method.getValue();

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		builder.queryParam("callback_query_id", callbackQueryId);
		if (showAlert)
			builder.queryParam("show_alert", true);
		if (text != null)
			builder.queryParam("text", text);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");

		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		restTemplate.postForEntity(builder.build().encode().toUri(), requestEntity, String.class);
	}
	
}
