package br.ufframework.telegram.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufframework.telegram.dto.CallbackQueryDTO;
import br.ufframework.telegram.dto.MessageDTO;
import br.ufframework.telegram.dto.UpdateDTO;
import br.ufframework.telegram.repository.UpdateRepository;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Component
public class UpdateHandler<T extends MessageReceiverTemplate> {

	@Autowired
	private T messageHandler;

	@Autowired
	private UpdateRepository updateRepository;

	public MessageDTO handleUpdate(UpdateDTO update) {
		updateRepository.save(update);

		CallbackQueryDTO callbackQuery = update.getCallbackQuery();
		if (callbackQuery != null) {
			messageHandler.handleCallbackQuery(callbackQuery);
			return null;
		}
		
		MessageDTO message = update.getMessage();
		if (message == null)
			message = update.getEditedMessage();
		return messageHandler.handleMessage(message);
	}

}
