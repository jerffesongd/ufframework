package br.ufframework.telegram.handler;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufframework.domain.Notificacao;
import br.ufframework.domain.Usuario;
import br.ufframework.notificacao.service.Notificador;
import br.ufframework.telegram.dto.ChatDTO;
import br.ufframework.telegram.repository.ChatRepository;

/**
 * Handler responsável por enviar mensagens ao Telegram.
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public class NotificadorTelegram extends AbstractMessageHandler implements Notificador {
	
	@Autowired
	private ChatRepository chatRepository;

	/**
	 * Envia uma mensagem ao Telegram, para o utimo chat do usuário externo referenciado.
	 */
	@Override
	public void notificarUsuarios(Notificacao notificacao, Collection<? extends Usuario> usuarios) {
		if (notificacao != null && usuarios != null) {
			for (Usuario usuario: usuarios) {
				ChatDTO chatAtivo = chatRepository.findLastChatByExternalUser(usuario.getId());
				if (chatAtivo != null)
					sendMessage(chatAtivo.getId(), notificacao.getMensagem(), null, null);	
			}
		}
	}
	
}
