package br.ufframework.telegram.handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufframework.domain.ErrorMessages;
import br.ufframework.domain.Grupo;
import br.ufframework.domain.Post;
import br.ufframework.domain.RespostaPost;
import br.ufframework.domain.States;
import br.ufframework.domain.StatusCadastro;
import br.ufframework.domain.UserContext;
import br.ufframework.domain.Usuario;
import br.ufframework.repository.PostRepository;
import br.ufframework.repository.RespostaPostRepository;
import br.ufframework.telegram.dto.CallbackQueryDTO;
import br.ufframework.telegram.dto.InlineKeyboardButtonDTO;
import br.ufframework.telegram.dto.InlineKeyboardMarkupDTO;
import br.ufframework.telegram.dto.MessageDTO;
import br.ufframework.telegram.repository.UserContextRepository;

/**
 * Handler responsável por tratar as mensagens recebidas do Telegram.
 * 
 * @author Lucas Aléssio
 *
 */
@Component
public abstract class MessageReceiverTemplate extends AbstractMessageHandler {

	@Autowired
	private UserContextRepository userContextRepository;

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private RespostaPostRepository respostaPostRepository;

	private UserContext userContext;

	private MessageDTO message;

	private CallbackQueryDTO callbackQuery;

	public MessageDTO handleMessage(MessageDTO message) {
		this.message = message;
		Integer chatId = message.getChat().getId();
		userContext = userContextRepository.findUserContextByIdUser(message.getUser().getId());

		if (message.getText().equals("/help"))
			return sendMessage(chatId, help(), null, null);		

		if (userContext == null) {
			if (message.getText().startsWith("/start") && message.getText().split(" ").length == 2) {
				associarNovoUsuario();
				return sendMessage(chatId, "Integração realizada!", null, null);
			} else
				return sendMessage(chatId, ErrorMessages.USUARIO_NAO_AUTENTICADO.getMessage(), null, null);
		}

		if (userContext != null && userContext.getIdGrupo() == null) {
			if (message.getText().equals(getGrupoCommand())) {
				return sendMessage(chatId, "Selecione:\n\n", null, markupSelecaoGrupos());
			} else
				return sendMessage(chatId, "Utilize o comando " + getGrupoCommand() + " para realizar alguma operação.", null, null);
		}

		if (userContext != null && userContext.getIdGrupo() != null) {
			if (message.getText().equals(getGrupoCommand()))
				return sendMessage(chatId, "Selecione:\n\n", null, markupSelecaoGrupos());

			if (userContext.getCurrentState().equals(States.GRUPO)) {
				if (message.getText().equals("/novo_post")) {
					updateUserState(States.POST);
					return sendMessage(chatId, infoCadastrarPost(), null, null);
				} else if (message.getText().equals("/responder_post")) {
					String postsMessage = listarPosts();
					if (postsMessage != null) {
						updateUserState(States.RESPOSTA_POST);
						return sendMessage(chatId, postsMessage, null, null);
					} else
						return sendMessage(chatId, ErrorMessages.GRUPO_NAO_TEM_POSTS.getMessage(), null, null);

				} else if (message.getText().equals("/posts"))
					return sendMessage(chatId, consultarPosts(), null, null);
				else
					return sendMessage(chatId, "Não entendi o que você disse. Digite /help para mais informações.", null, null);
			} else if (userContext.getCurrentState().equals(States.POST)) {
				if (cadastrarPost()) {
					updateUserState(States.GRUPO);
					return sendMessage(chatId, "Post cadastrado!!!", null, null);
				} else
					return sendMessage(chatId, "Ocorreu algum erro. Tente novamente cadastrar o post.", null, null);
			} else if (userContext.getCurrentState().equals(States.RESPOSTA_POST)) {
				if (responderPost()) {
					updateUserState(States.GRUPO);
					return sendMessage(chatId, "Post respondido!!!", null, null);
				} else
					return sendMessage(chatId, "Ocorreu algum erro. Tente novamente responder o post.", null, null);
			}
		}

		return null;
	}
	
	public void handleCallbackQuery(CallbackQueryDTO callbackQuery) {
		this.callbackQuery = callbackQuery;

		if (definirGrupo()) {
			updateUserState(States.GRUPO);
			answerCallbackQuery(callbackQuery.getId(), callbackQuery.getData() + " selecionado!", false);
			sendMessage(message.getChat().getId(), "Agora você já pode utilizar os comandos /posts, /novo_post e /responder_post.", null, null);
		} else
			sendMessage(message.getChat().getId(), ErrorMessages.CODIGO_INVALIDO.getMessage(), null, null);
	}

	/**
	 * Cria o contéudo dos botões com as opções de grupos, para o usuário escolher.
	 * 
	 * @return
	 */
	public String markupSelecaoGrupos() {
		Collection<? extends Grupo> grupos = consultarGrupos(userContext.getIdUsuarioExterno());
		Iterator<? extends Grupo> iterator = grupos.iterator();

		InlineKeyboardMarkupDTO markup = new InlineKeyboardMarkupDTO();
		List<List<InlineKeyboardButtonDTO>> buttonsAll = new ArrayList<List<InlineKeyboardButtonDTO>>();;
		while (iterator.hasNext()) {
			List<InlineKeyboardButtonDTO> buttonsColumns = new ArrayList<InlineKeyboardButtonDTO>();
			for (int i=0; i<3; i++) {
				if (iterator.hasNext()) {
					InlineKeyboardButtonDTO but = new InlineKeyboardButtonDTO();
					String codigo = iterator.next().getCodigo();
					but.setText(codigo);
					but.setCallbackData(codigo);
					buttonsColumns.add(but);
				}
			}
			buttonsAll.add(buttonsColumns);
		}
		InlineKeyboardButtonDTO[][] buttons = new InlineKeyboardButtonDTO[buttonsAll.size()][3];
		for (int i=0; i< buttonsAll.size(); i++) {
			buttons[i] = buttonsAll.get(i).toArray(new InlineKeyboardButtonDTO[buttonsAll.get(i).size()]);
		}
		markup.setButtons(buttons);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(markup);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String listarGrupos() {
		Collection<? extends Grupo> grupos = consultarGrupos(userContext.getIdUsuarioExterno());
		if (!grupos.isEmpty()) {
			StringBuilder resposta = new StringBuilder();
			for (Grupo grupo: grupos) {
				resposta.append("<strong>")
						.append(grupo.getCodigo())
						.append("</strong> - " )
						.append(grupo.getTitulo())
						.append("\n");
			}
			return resposta.toString();
		} else
			return ErrorMessages.ERROR.getMessage();
	}

	public String consultarPosts() {
		Collection<Post> posts = postRepository.findByIdGrupo(userContext.getIdGrupo(), StatusCadastro.CADASTRADO.getValue());
		if (!posts.isEmpty()) {
			String resposta = "<strong>Últimos posts (" + posts.size() + "):</strong>\n\n";
			for (Post post: posts)
				resposta += ("[" + post.getTitulo() + "] " + post.getConteudo() + "\n");
			return resposta;
		} else
			return ErrorMessages.USUARIO_NAO_CADASTROU_POSTS.getMessage();
	}

	public String help() {
		return "Possíveis comandos:\n" +
				getGrupoCommand() + " - " + getDescricaoGrupoCommand() + "\n" + 
				"/posts - Lista os posts cadastrados na turma selecionada\n" + 
				"/novo_post - Cadastra um post na turma\n" + 
				"/responder_post - Responde um post da turma";
	}

	public String infoCadastrarPost() {
		return "Defina o título e conteúdo do post da seguinte maneira:\n\n"
				+ "[<i>título</i>] <i>conteúdo do post</i>";
	}

	public String listarPosts() {
		Collection<Post> posts = postRepository.findByIdGrupo(userContext.getIdGrupo(), StatusCadastro.CADASTRADO.getValue());
		if (!posts.isEmpty()) {
			String resposta = "Selecione um dos post abaixo e responda da seguinte maneira:\n\n"
					+ "[<i>número do post</i>] <i>conteúdo da resposta</i>\n\n";
			int counter=1;
			for (Post post: posts)
				resposta += ("#" + counter + " " + post.getTitulo() + "\n");
			return resposta;
		} else
			return null;
	}

	public boolean definirGrupo() {
		Collection<? extends Grupo> grupos = consultarGrupos(userContext.getIdUsuarioExterno());
		for (Grupo grupo: grupos) {
			if (grupo.getCodigo().contains(callbackQuery.getData())) {
				userContext.setIdGrupo(grupo.getId());
				userContextRepository.save(userContext);
				return true;
			}
		}		
		return false;
	}

	public boolean cadastrarPost() {
		try {
			String titulo = message.getText().substring(message.getText().indexOf("[") + 1, message.getText().indexOf("]")).trim();
			String conteudo = message.getText().substring(message.getText().indexOf("]") + 1).trim();

			if (!(titulo.isEmpty() || conteudo.isEmpty())) {
				Post post = new Post();
				Usuario responsavel = new Usuario();
				responsavel.setIdUsuario(userContext.getIdUsuarioExterno());
				Grupo grupo = new Grupo();
				grupo.setId(userContext.getIdGrupo());
				
				post.setResponsavel(responsavel);
				post.setGrupo(grupo);
				post.setStatus(StatusCadastro.CADASTRADO.getValue());
				post.setConteudo(conteudo);
				post.setTitulo(titulo);
				postRepository.save(post);
				return true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return false;
	}

	public boolean responderPost() {
		try {
			Integer index = Integer.valueOf(message.getText().substring(message.getText().indexOf("[") + 1, message.getText().indexOf("]")));
			String conteudo = message.getText().substring(message.getText().indexOf("]") + 1).trim();

			if (!conteudo.isEmpty()) {
				RespostaPost resposta = new RespostaPost();
				Usuario responsavel = new Usuario();
				responsavel.setIdUsuario(userContext.getIdUsuarioExterno());
				resposta.setResponsavel(responsavel);
				resposta.setStatus(StatusCadastro.CADASTRADO.getValue());
				Collection<Post> posts = postRepository.findByIdGrupo(userContext.getIdGrupo(), StatusCadastro.CADASTRADO.getValue());
				if (!posts.isEmpty()) {
					Post post = (Post) posts.toArray()[index-1];
					resposta.setPost(post);
					resposta.setConteudo(conteudo);
					respostaPostRepository.save(resposta);
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return false;
	}

	private void associarNovoUsuario() {
		Integer idUsuarioExterno = Integer.valueOf(message.getText().split(" ")[1]);
		userContext = new UserContext();
		userContext.setIdUser(message.getUser().getId());
		userContext.setIdUsuarioExterno(idUsuarioExterno);
		userContextRepository.save(userContext);
	}

	private void updateUserState(States newState) {
		if (userContext != null) {
			userContext.setCurrentState(newState);
			userContextRepository.save(userContext);
		}
	}
	
	/**
	 * Consulta o conjunto de grupos do usuário informado.
	 * 
	 * @param idUsuarioExterno
	 * @return
	 */
	public abstract Collection<? extends Grupo> consultarGrupos(int idUsuarioExterno);
	
	/*
	 * Define o comando a ser utilizado para definição do grupo.
	 */
	public abstract String getGrupoCommand();
	
	/*
	 * Define a descrição do comando a ser utilizado para definição do grupo.
	 */
	public abstract String getDescricaoGrupoCommand();

}
