package br.ufframework.telegram.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.ufframework.telegram.dto.MessageDTO;
import br.ufframework.telegram.dto.UpdateDTO;
import br.ufframework.telegram.handler.MessageReceiverTemplate;
import br.ufframework.telegram.handler.UpdateHandler;

/**
 * Controller responsável pela comunicação com o bot no Telegram
 * 
 * @author Lucas Alessio
 *
 */
@Component
public class TelegramController<T extends MessageReceiverTemplate> {

	@Autowired
	private UpdateHandler<T> service;

	@PostMapping("/api/telegram/webhook")
	public MessageDTO getUpdate(@RequestBody UpdateDTO update) {
		return service.handleUpdate(update);
	}

}
