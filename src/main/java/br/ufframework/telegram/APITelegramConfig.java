package br.ufframework.telegram;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Component
@PropertySource("classpath:apitelegram.properties")
public class APITelegramConfig {

	@Value("${url-api-telegram}")
	private String urlBase;

	@Value("${api-key}")
	private String key;

	public String getUrlBase() {
		return urlBase;
	}

	public void setUrlBase(String urlBase) {
		this.urlBase = urlBase;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
