package br.ufframework.helper;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.ufframework.domain.Usuario;


@ControllerAdvice
@Component
public class AbstractUsuarioHelper<T extends Usuario> {

	private T usuarioLogado;
	
	@ModelAttribute("usuarioLogado")
	public T getUsuarioLogado() {
		return this.usuarioLogado;
	}
	
	public void setUsuarioLogado(T usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
	
	
	
}
