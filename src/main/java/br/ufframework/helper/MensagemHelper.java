package br.ufframework.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.ufframework.domain.Mensagem;


/**
 * Helper para envio de mensagens para usuarios do sistema.
 * @author luciooliveira
 *
 */
@Component
public final class MensagemHelper {
	
	@Autowired
	private MessageSource messageSource;
	
	private MensagemHelper() {
	}

	public void sucesso(RedirectAttributes ra, String message, Object... args) {
		addAtributo(ra, message, Mensagem.Tipo.SUCCESS, args);
	}

	public void erro(RedirectAttributes ra, String message, Object... args) {
		addAtributo(ra, message, Mensagem.Tipo.DANGER, args);
	}

	public void informacao(RedirectAttributes ra, String message, Object... args) {
		addAtributo(ra, message, Mensagem.Tipo.INFO, args);
	}

	public void cuidado(RedirectAttributes ra, String message, Object... args) {
		addAtributo(ra, message, Mensagem.Tipo.WARNING, args);
	}

	private void addAtributo(RedirectAttributes ra, String message, Mensagem.Tipo type, Object... args) {
		String i18nMensagem = messageSource.getMessage(message, args, LocaleContextHolder.getLocale());
		ra.addFlashAttribute(Mensagem.LAYOUT_ATRIBUTO_MENSAGEM, new Mensagem(i18nMensagem, type, args));
	}

	public void sucesso(Model model, String message, Object... args) {
		addAtributo(model, message, Mensagem.Tipo.SUCCESS, args);
	}

	public void erro(Model model, String message, Object... args) {
		addAtributo(model, message, Mensagem.Tipo.DANGER, args);
	}

	public void informacao(Model model, String message, Object... args) {
		addAtributo(model, message, Mensagem.Tipo.INFO, args);
	}

	public void cuidado(Model model, String message, Object... args) {
		addAtributo(model, message, Mensagem.Tipo.WARNING, args);
	}

	private void addAtributo(Model model, String message, Mensagem.Tipo type, Object... args) {
		String i18nMensagem = messageSource.getMessage(message, args, LocaleContextHolder.getLocale());
		model.addAttribute(Mensagem.LAYOUT_ATRIBUTO_MENSAGEM, new Mensagem(i18nMensagem, type, args));
	}

}