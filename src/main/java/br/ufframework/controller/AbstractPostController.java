package br.ufframework.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.domain.Grupo;
import br.ufframework.domain.Post;
import br.ufframework.domain.RespostaPost;
import br.ufframework.helper.MensagemHelper;
import br.ufframework.helper.AbstractUsuarioHelper;
import br.ufframework.repository.GrupoRepository;
import br.ufframework.repository.PostRepository;
import br.ufframework.repository.RespostaPostRepository;


@Controller("/post")
public abstract class AbstractPostController {

	
	@ModelAttribute("post")
	public Post createPost() {
	    return new Post();
	}
	
	@ModelAttribute("respostaPost")
	public RespostaPost createRespostaPost() {
	    return new RespostaPost();
	}

	@Autowired
	protected AbstractUsuarioHelper usuarioHelper;
	
	@Autowired
	protected GrupoRepository grupoRepository;
	
	@Autowired
	protected MensagemHelper mensagemHelper;
	
	@Autowired
	protected PostRepository postRepository;
	
	@Autowired
	protected RespostaPostRepository respostaPostRepository;

	protected int idGrupo;
	protected int idPost;
	protected int idRespostaPost;
	
    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public abstract ModelAndView salvar(@Valid Post post, BindingResult result, RedirectAttributes ra);

	@GetMapping(value= {"/form_novo_post/{idGrupo}", "/{idGrupo}/form_novo_post"})
	public ModelAndView novoPost(@PathVariable Integer idGrupo) {
		ModelAndView modelAndView = new ModelAndView("post_form");
		this.idGrupo = idGrupo;
		return modelAndView;
	}
	
	@GetMapping(value= {"/form_editar_post/{idPost}", "/{idPost}/form_editar_post"})
	public ModelAndView editarPost(@PathVariable Integer idPost) {
		ModelAndView modelAndView = new ModelAndView("post_form");
		Post post = postRepository.buscarPorId(idPost);
		this.idPost = post.getId();
		this.idGrupo  = post.getGrupo().getId();
		modelAndView.addObject("post", post);
		return modelAndView;
	}	

	@GetMapping(value= {"/visualizar/{idPost}", "/{idPost}/visualizar"})
	public ModelAndView visualizar(@PathVariable Integer idPost) {
		ModelAndView modelAndView = new ModelAndView("post_visualizacao");
		Post post = postRepository.buscarPorId(idPost);
		
		if(post.getVisualizacoes() == null) {
			post.setVisualizacoes(0);
		}
		
		post.setVisualizacoes(post.getVisualizacoes() + 1);
		postRepository.save(post);
		
		this.idGrupo = post.getId();
		this.idPost = idPost;
		
		
		
		modelAndView.addObject("post", post);
		modelAndView.addObject("grupo", grupoRepository.findOne(idGrupo));
		return modelAndView;
	}
	

	@GetMapping(value= {"/remover_post/{idPost}", "/{idPost}/remover_post"})
	public ModelAndView removerPost(@PathVariable Integer idPost) {
		
		Post p = new Post();
		p = postRepository.findOne(idPost);
		idGrupo = p.getGrupo().getId();
		List<RespostaPost> repostas = respostaPostRepository.buscarPorPost(idPost);
		respostaPostRepository.deleteInBatch(repostas);	
		postRepository.delete(idPost);
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/post/"+idGrupo, true));
		return modelAndView;
	}
	
	@GetMapping(value= {"/remover_resposta/{idRespostaPost}/{idPost}", "/{idRespostaPost}/{idPost}/remover_resposta"})
	public ModelAndView removerResposta(@PathVariable Integer idRespostaPost, @PathVariable Integer idPost) {
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/post/visualizar/"+idPost, true));
		
		respostaPostRepository.delete(idRespostaPost);

		return modelAndView;
	}
	
	@GetMapping(value= {"/editar_resposta/{idRespostaPost}/{idPost}", "/{idRespostaPost}/{idPost}/editar_resposta"})
	public ModelAndView ediarResposta(@PathVariable Integer idRespostaPost, @PathVariable Integer idPost) {
		ModelAndView modelAndView = new ModelAndView("editar_resposta_form");
		this.idPost = idPost;
		this.idRespostaPost = idRespostaPost;
		RespostaPost resposta = respostaPostRepository.buscarPorId(idRespostaPost); 
		modelAndView.addObject("respostaPost", resposta);
		return modelAndView;
	}
	
    @RequestMapping(value = "/salvar_edicao_resposta", method = RequestMethod.POST)
    public ModelAndView responder(@Valid RespostaPost respostaPost, BindingResult result, RedirectAttributes ra) {
		ModelAndView modelAndView = new ModelAndView("post_visualizacao");
		
		Post post = postRepository.buscarPorId(this.idPost);
		
		RespostaPost resposta = respostaPostRepository.buscarPorId(this.idRespostaPost); 
		resposta.setConteudo(respostaPost.getConteudo());
		respostaPostRepository.save(resposta);

		List<RespostaPost> respostas = respostaPostRepository.buscarPorPost(post.getId());
		
		modelAndView.addObject("respostas", respostas);
		modelAndView.addObject("post", post);

		modelAndView.addObject("postController", this);

		return modelAndView;
	}
	
    @RequestMapping(value = "/responder/{idPost}", method = RequestMethod.POST)
    public ModelAndView responder(@PathVariable Integer idPost, @Valid RespostaPost respostaPost, BindingResult result, RedirectAttributes ra) {
    	ModelAndView modelAndView = new ModelAndView(new RedirectView("/post/visualizar/"+idGrupo, true));
		
		Post post = postRepository.buscarPorId(idPost);

		respostaPost.setDataCriacao(new Date());
		
		respostaPost.setPost(post);
		respostaPost.setResponsavel(usuarioHelper.getUsuarioLogado());
		respostaPostRepository.save(respostaPost);
		modelAndView.addObject("respostaPost", new RespostaPost());

		return modelAndView;
	}
    
    

	@GetMapping(value= {"/{idGrupo}", "/{idGrupo}/listar"})
	public ModelAndView listar(@PathVariable Integer idGrupo) {
		this.idGrupo = idGrupo;
		ModelAndView modelAndView = new ModelAndView("posts");

		Grupo grupo = grupoRepository.findOne(idGrupo);
		
		modelAndView.addObject("grupo", grupo);
		
		modelAndView.addObject("posts", postRepository.buscarPorGrupo(idGrupo));
		
		return modelAndView;
	}

	
	//Botões de voltar
	@GetMapping(value= {"/voltar_para_posts", "/voltar_para_posts"})
	public ModelAndView voltarParaPosts() {
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/post/"+idGrupo, true));
		
		return modelAndView;
	}
	
	@GetMapping(value= {"/voltar_para_turmas", "/voltar_para_turmas"})
	public ModelAndView voltarParaIndex() {
		ModelAndView modelAndView = new ModelAndView(new RedirectView("/turmas", true));
		return modelAndView;
	}
	
	@GetMapping(value= {"/voltar_visualizar_post", "/voltar_visualizar_post"})
	public ModelAndView voltarParaVisualizarPost() {
		ModelAndView modelAndView = new ModelAndView("post_visualizacao");
		
		Post post = postRepository.buscarPorId(idPost);
		modelAndView.addObject("postController", this);
		
		modelAndView.addObject("post", post);
		
		return modelAndView;
	}
	
	protected String removerCaracteresEspeciais(String string) {
		
		string = string.replaceAll("[áÁàÀâÂãÃ]", "a")
				.replaceAll("[éÉèÈêÊẽẼ]", "e")
				.replaceAll("[íÍìÌîÎĩĨ]", "i")
				.replaceAll("[óÓòÒôÔõÕ]", "o")
				.replaceAll("[úÚùÙûÛũŨ]", "u")
				.replaceAll("[çÇ]", "c")
				.replaceAll("[ñÑ]", "n");
		
		return string;
	}

}

