package br.ufframework.controller;

import java.util.ArrayList;
import java.util.Date;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.ufframework.service.AbstractGeraRelatorio;

@Controller("/relatorio")
public abstract class AbstractRelatorioController{
	
	private AbstractGeraRelatorio geraRelatorio;

	public AbstractRelatorioController() {
	}	

	public AbstractGeraRelatorio getGeraRelatorio() {
		return geraRelatorio;
	}

	public void setGeraRelatorio(AbstractGeraRelatorio geraRelatorio) {
		this.geraRelatorio = geraRelatorio;
	}

	@GetMapping(value= {"/mostrar_relatorio"})
	public ModelAndView mostrarRelatorio() {
		ModelAndView modelAndView = new ModelAndView("relatorio_visualizacao");
		
		Map<String, Integer> pares = geraRelatorio.gerar();

		List<String> rotulos = new ArrayList<String>();
		List<Integer> valores = new ArrayList<Integer>();
		
		for (Map.Entry<String,Integer> pair : pares.entrySet()) {
			rotulos.add(pair.getKey());
		    valores.add(pair.getValue());
		}

		modelAndView.addObject("rotulos", rotulos);
		modelAndView.addObject("valores", valores);
		modelAndView.addObject("pares", pares);

		return modelAndView;
	}
}

