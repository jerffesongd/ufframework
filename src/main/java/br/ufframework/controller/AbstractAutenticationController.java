package br.ufframework.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author Lucas Alessio
 *
 */
@Controller
public abstract class AbstractAutenticationController {

	@GetMapping("/autenticacao")
	abstract public String formAutenticacao(Model model);


	@GetMapping("/logout")
	public abstract ModelAndView logout();


}
