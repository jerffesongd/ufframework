package br.ufframework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.domain.Post;

public interface PostRepository extends JpaRepository<Post,Integer> {
	
	@Query(value = "select * from post p where p.id_grupo = ?1", nativeQuery=true)
	List<Post> buscarPorGrupo(int idTurma);
	
	@Query(value = "select * from post p where p.id_post = ?1", nativeQuery=true)
	Post buscarPorId(int idPost);
	
	@Query(value="from Post where idResponsavel = :idResponsavel and status = :status order by dataCriacao desc ")
	List<Post> findByResponsavel(@Param("idResponsavel") Integer idResponsavel, @Param("status") String status);
	
	@Query(value="from Post where grupo.id = :idGrupo and status = :status order by dataCriacao desc ")
	List<Post> findByIdGrupo(@Param("idGrupo") Integer idTurmaSigaa, @Param("status") String status);
	
	@Query(value="from Post where idResponsavel = :idResponsavel and idGrupo = :idTurmaSigaa and status = :status order by dataCriacao desc ")
	List<Post> findByResponsavelAndIdGrupo(@Param("idResponsavel") Integer idResponsavel, @Param("idTurmaSigaa") Integer idTurmaSigaa, @Param("status") String status);
	
	@Query(value = "select * from post p order by DATA_CRIACAO desc", nativeQuery=true)
	List<Post> findAllByDataCriacao();
	
}
