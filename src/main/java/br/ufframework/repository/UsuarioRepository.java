package br.ufframework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.domain.Post;
import br.ufframework.domain.Usuario;

public interface UsuarioRepository<T extends Usuario> extends JpaRepository<T,Integer> {
	
	@Query(value = "select * from usuario u where u.login like ?1", nativeQuery=true)
	T findByLogin(String login);
	
}
