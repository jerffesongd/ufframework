package br.ufframework.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import br.ufframework.domain.AbstractEntity;


@NoRepositoryBean
public interface AbstractRepository<T extends AbstractEntity> extends JpaRepository<T,Integer> {
	@Override
	@Query(value = "select * from #{#entityName} where ativo = true", nativeQuery=true)
	List<T> findAll();
	
	@Query(value = "select * from #{#entityName} where id_#{#entityName} = ?1 and ativo = true", nativeQuery=true)
	T findOne(Integer arg0);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE #{#entityName} SET ativo=false where id_#{#entityName} = ?1", nativeQuery=true)
	void delete(Integer arg0);	
	
	@Override
	default void delete(T arg0) {
		// TODO Auto-generated method stub
		delete(arg0.getId());
	}
	
	@Override
	default void deleteInBatch(Iterable<T> arg0) {
		// TODO Auto-generated method stub
		arg0.forEach(arg -> delete(arg));
	}

}
