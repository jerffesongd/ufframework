package br.ufframework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufframework.domain.RespostaPost;

public interface RespostaPostRepository extends JpaRepository<RespostaPost, Integer> {
	
	@Query(value = "select * from resposta_post r where r.id_post = ?1", nativeQuery=true)
	List<RespostaPost> buscarPorPost(int idPost);
	
	@Query(value="from RespostaPost where idResponsavel = :idResponsavel and status = :status order by dataCriacao desc ")
	List<RespostaPost> findByResponsavel(@Param("idResponsavel") Integer idResponsavel, @Param("status") String status);
	
	
	@Query(value = "select * from resposta_post r where r.id_resposta_post = ?1", nativeQuery=true)
	RespostaPost buscarPorId(int idResposta);

}
