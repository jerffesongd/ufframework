package br.ufframework.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ufframework.domain.Grupo;


public interface GrupoRepository extends JpaRepository<Grupo,Integer> {

}
